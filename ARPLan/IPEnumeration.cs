﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using ARPScanSubNet;

namespace ARPLan
{
    public class IPEnumeration : IEnumerable
    {
        private readonly string _endAddress;
        private readonly string _startAddress;

        public IPEnumeration(string startAddress, string endAddress)
        {
            _startAddress = startAddress;
            _endAddress = endAddress;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public Int64 Count
        {
            get { return AddressToInt(_endAddress) - AddressToInt(_startAddress); }
        }
        private static Int64 AddressToInt(IPAddress addr)
        {
            byte[] addressBits = addr.GetAddressBytes();

            Int64 retval = 0;
            for (int i = 0; i < addressBits.Length; i++)
            {
                retval = (retval << 8) + addressBits[i];
            }

            return retval;
        }

        internal static Int64 AddressToInt(string addr)
        {
            return AddressToInt(IPAddress.Parse(addr));
        }

        internal static IPAddress IntToAddress(Int64 addr)
        {
            return IPAddress.Parse(addr.ToString());
        }


        public IPEnumerator GetEnumerator()
        {
            return new IPEnumerator(_startAddress, _endAddress);
        }
    }
    public class IPEnumerator : IEnumerator
    {
        private readonly Int64 _endIp;
        private readonly string _startAddress;
        private Int64 _currentIp;

        public IPEnumerator(string startAddress, string endAddress)
        {
            _startAddress = startAddress;

            _currentIp = IPEnumeration.AddressToInt(startAddress);
            _endIp = IPEnumeration.AddressToInt(endAddress);
        }

        public IPAddress Current
        {
            get
            {
                try
                {
                    return IPEnumeration.IntToAddress(_currentIp);
                }
                catch (IndexOutOfRangeException)
                {
                    throw new InvalidOperationException();
                }
            }
        }

        public bool MoveNext()
        {
            _currentIp++;
            return (_currentIp <= _endIp);
        }

        public void Reset()
        {
            _currentIp = IPEnumeration.AddressToInt(_startAddress);
        }

        object IEnumerator.Current
        {
            get { return Current; }
        }
    }
}
