﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;

namespace ARPLan
{
    public static class IPHelper
    {

        #region Constants

        private const int NO_ERROR = 0;
        // Maximum length of a physical address
        private const int PHYSADDR_MAXLEN = 8;
        // Insufficient buffer error
        private const int INSUFFICIENT_BUFFER = 122;

        #endregion

        #region Structs

        // Define the MIB_IPNETROW structure.
        [StructLayout(LayoutKind.Sequential)]
        private struct MIB_IPNETROW
        {
            [MarshalAs(UnmanagedType.U4)]
            public readonly int dwIndex;
            [MarshalAs(UnmanagedType.U4)]
            public readonly int dwPhysAddrLen;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = PHYSADDR_MAXLEN)]
            public readonly byte[] bPhysAddr;
            [MarshalAs(UnmanagedType.U4)]
            public readonly int dwAddr;
            [MarshalAs(UnmanagedType.U4)]
            public readonly int dwType;
        }

        #endregion

        #region WinApi
        /// <summary>
        /// http://msdn.microsoft.com/en-us/library/windows/desktop/aa366358(v=vs.85).aspx
        /// </summary>
        /// <param name="destIp">The destination IPv4 address, in the form of an IPAddr structure. The ARP request attempts to obtain the physical address that corresponds to this IPv4 address.</param>
        /// <param name="srcIp">The source IPv4 address of the sender, in the form of an IPAddr structure. This parameter is optional and is used to select the interface to send the request on for the ARP entry. The caller may specify zero corresponding to the INADDR_ANY IPv4 address for this parameter.</param>
        /// <param name="pMacAddr">A pointer to an array of ULONG variables. This array must have at least two ULONG elements to hold an Ethernet or token ring physical address. The first six bytes of this array receive the physical address that corresponds to the IPv4 address specified by the DestIP parameter.</param>
        /// <param name="phyAddrLen">On input, a pointer to a ULONG value that specifies the maximum buffer size, in bytes, the application has set aside to receive the physical address or MAC address. The buffer size should be at least 6 bytes for an Ethernet or token ring physical address</param>
        /// <returns>If the function succeeds, the return value is NO_ERROR.</returns>
        [DllImport("iphlpapi.dll", ExactSpelling = true)]
        public static extern int SendARP(int destIp, int srcIp, byte[] pMacAddr, ref uint phyAddrLen);



        /// <summary>
        /// The GetIpNetTable function retrieves the IP-to-physical address mapping table.
        /// </summary>
        /// <param name="pIpNetTable">A pointer to a buffer that receives the
        ///        IP-to-physical address mapping table as a MIB_IPNETTABLE structure.</param>
        /// <param name="pdwSize">On input, specifies the size of the buffer pointed to
        /// by the pIpNetTable parameter.
        /// <para>On output, if the buffer is not large enough to hold the returned mapping table,
        /// the function sets this parameter equal to the required buffer size</para></param>
        /// <param name="bOrder">A Boolean value that specifies whether the returned mapping
        /// table should be sorted in ascending order by IP address. If this parameter is TRUE,
        /// the table is sorted.</param>
        /// <returns>If the function succeeds, the return value is NO_ERROR.
        /// <para>If the function fails, the return value is one of the following error codes:
        /// ERROR_INSUFFICIENT_BUFFER, ERROR_INVALID_PARAMETER, ERROR_NOT_SUPPORTED or other code.
        /// </para>
        /// </returns>
        [DllImport("Iphlpapi.dll", EntryPoint = "GetIpNetTable")]
        internal static extern int GetIpNetTable(IntPtr pIpNetTable, ref int pdwSize, bool bOrder);
        #endregion

        #region Public methods
        /// <summary>
        /// Gets the hardware ethernet address of the given IP address as an string. The address is searched
        /// for in the ARP table cache and (if not found there) and ARP request is transmitted to find out the address.
        /// </summary>
        /// <param name="address">IP address to find the hardware ethernet address for</param>
        /// <returns>The hardware ethernet address</returns>
        public static string GetHardwareAddress(IPAddress address)
        {

            MIB_IPNETROW[] addrTable = GetPhysicalAddressTable();
            if (addrTable != null)
            {
                for (int i = 0; i < addrTable.Length; i++)
                {
                    byte[] addr = address.GetAddressBytes();
                    if (BitConverter.ToInt32(addr, 0) == addrTable[i].dwAddr)
                    {
                        byte[] physAddr = new byte[addrTable[i].dwPhysAddrLen];
                        Array.Copy(addrTable[i].bPhysAddr, physAddr, addrTable[i].dwPhysAddrLen);
                        return  ConvertByteMacToString(physAddr);
                    }
                }
            }
            return SendArpRequest(address);
        }
        #endregion

        #region Private (lowlevel) methods


        private static string SendArpRequest(IPAddress address)
        {
            int intAddress = BitConverter.ToInt32(address.GetAddressBytes(), 0);

            var macAddr = new byte[6];
            var macAddrLen = (uint)macAddr.Length;
            if (SendARP(intAddress, 0, macAddr, ref macAddrLen) != NO_ERROR)
                return "(NO ARP result)";
            return ConvertByteMacToString(macAddr);
        }

        private static MIB_IPNETROW[] GetPhysicalAddressTable()
        {
            int bytesNeeded = 0;
            int result = GetIpNetTable(IntPtr.Zero, ref bytesNeeded, false);

            if (result != INSUFFICIENT_BUFFER)
            {
                throw new ApplicationException(Convert.ToString(result));
            }

            IntPtr buffer = IntPtr.Zero;
            MIB_IPNETROW[] table;

            try
            {
                buffer = Marshal.AllocCoTaskMem(bytesNeeded);

                result = GetIpNetTable(buffer, ref bytesNeeded, false);

                if (result != 0)
                {
                    throw new ApplicationException(Convert.ToString(result));
                }

                int entries = Marshal.ReadInt32(buffer);
                IntPtr currentBuffer = new IntPtr(buffer.ToInt64() + sizeof(int));
                table = new MIB_IPNETROW[entries];

                for (int i = 0; i < entries; i++)
                {
                    table[i] = (MIB_IPNETROW)Marshal.PtrToStructure(
                                               new IntPtr(currentBuffer.ToInt64() + (i * Marshal.SizeOf(typeof(MIB_IPNETROW)))),
                                               typeof(MIB_IPNETROW)
                                               );
                }
            }
            finally
            {
                Marshal.FreeCoTaskMem(buffer);
            }
            return table;
        }
        #endregion

        #region Utility
        private static string ConvertByteMacToString(byte[] macAddr)
        {
            var macAddrLen = (uint)macAddr.Length;
            var str = new string[(int)macAddrLen];
            for (int i = 0; i < macAddrLen; i++)
                str[i] = macAddr[i].ToString("x2");
            return string.Join("-", str);
        }
        #endregion
    }
}
