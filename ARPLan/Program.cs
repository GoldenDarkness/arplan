﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using ARPLan;

namespace ARPScanSubNet
{

    public static class Program
    {
        public static void Main(string[] args)
        {
            var pingTasks = new List<Task<PingReply>>();
            var range = new IPEnumeration("192.168.0.0", "192.168.0.255");
            Console.Write("Pinging... ");
            using (var progress = new ProgressBar())
            {
                double i = 0;
                foreach (IPAddress addr in range)
                {
                    pingTasks.Add(PingAsync(addr));
                    i++;
                    progress.Report(i / range.Count);
                }
            }
            
            Task.WaitAll(pingTasks.ToArray<Task>());
            Console.WriteLine("Pinging completed\n");

            var top = Console.CursorTop;
            var left = Console.CursorLeft;
            var index = 0;
            var table = new ConsoleTable("Index", "IP Address", "MAC Address");


            foreach (var pingTask in pingTasks)
            {
                if (pingTask.Result.Status == IPStatus.Success)
                {
                    Console.SetCursorPosition(left, top);
                    index++;
                    table.AddRow(index, pingTask.Result.Address, IPHelper.GetHardwareAddress(pingTask.Result.Address));
                    table.Write();
                }
            }
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey(true);
        }

        private static Task<PingReply> PingAsync(IPAddress address)
        {
            var tcs = new TaskCompletionSource<PingReply>();
            using (var ping = new Ping())
            {
                const string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
                byte[] buffer = Encoding.ASCII.GetBytes(data);
                const int timeout = 150;
                ping.PingCompleted += (obj, sender) => tcs.SetResult(sender.Reply);
                ping.SendAsync(address, timeout, buffer);
            }
            return tcs.Task;
        }
    }
}
